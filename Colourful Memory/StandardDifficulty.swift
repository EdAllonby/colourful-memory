import UIKit

/// The standard difficulty decider. Asymptotically reaches 9 tile colours and 1 second wait time.
class StandardDifficulty : DifficultyDecider
{
    private let startingWaitTime: Double = 1.5
    private let waitTimeExponent: Double = -0.3
    private let finalColouredTileCount: Int = 9
    
    /// Calculates how long of a wait a round should have and how many tiles should be coloured
	func updateDifficulty(roundNumber: Int) -> Difficulty
	{
        
		var waitTime = calculateWaitTime(roundNumber)
        var colouredTiles = calculateColouredTilesNumber(roundNumber)
        var coloursForTiles = calculateColoursForTiles(roundNumber)
        
        return Difficulty(waitTime: waitTime, colouredTiles: colouredTiles, coloursForTiles: coloursForTiles)
	}
    
    private func calculateWaitTime(roundNumber: Int) -> Double
    {
        return startingWaitTime * pow(Double(roundNumber), waitTimeExponent)
    }
    
    private func calculateColouredTilesNumber(roundNumber: Int) -> Int
    {
        var value = (-7 * exp(-0.04 * Double(roundNumber))) + Double(finalColouredTileCount)
        return Int(value)
    }

    private func calculateColoursForTiles(roundNumber: Int) -> TileColours
    {
        // TODO: Should TileColours be recreated each round, or each game (reset)?
        var tileColours = TileColours()

        if(roundNumber >= 5)
        {
            tileColours.addNextColour()
        }
        if(roundNumber >= 15)
        {
            tileColours.addNextColour()
        }

        return tileColours
    }
}