//
//  MessageDisplayer.swift
//  Colourful Memory
//
//  Created by Ed Allonby on 24/12/2014.
//  Copyright (c) 2014 Ed Allonby. All rights reserved.
//

import Foundation

/// Swift doesn't have events. Use a pseudo-observer pattern
protocol MessageDisplayer
{
    /// Displays a new UI Message
    func displayNewMessage(message: String)
 
    /// Displays the highest round on the UI
    func displayHighestRoundMessage(message: String)
    
    /// Update the current peeks left
    func displayCurrentPeeks(peekCount: Int)
}