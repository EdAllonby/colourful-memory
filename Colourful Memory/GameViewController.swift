//
//  ViewController.swift
//  Colourful Memory
//
//  Created by Ed Allonby on 15/12/2014.
//  Copyright (c) 2014 Ed Allonby. All rights reserved.
//

import UIKit
import AVFoundation

class GameViewController: UIViewController, AVAudioPlayerDelegate, MessageDisplayer
{
	@IBOutlet var button1: UIButton!
	@IBOutlet var button2: UIButton!
	@IBOutlet var button3: UIButton!
	@IBOutlet var button4: UIButton!
	@IBOutlet var button5: UIButton!
	@IBOutlet var button6: UIButton!
	@IBOutlet var button7: UIButton!
	@IBOutlet var button8: UIButton!
	@IBOutlet var button9: UIButton!

	@IBOutlet var userMessageLabel: UILabel!
    
    @IBOutlet var highestRoundLabel: UILabel!
    
    @IBOutlet var peekCount: UIButton!
    
    var game: Game?
    
    var player : AVAudioPlayer! = nil

	override func viewDidLoad()
	{
        // Keeps background audio player (Such as device Music)
        AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient, error: nil) == true

		super.viewDidLoad()

        // Makes overlapping animations start from last animation point.
        UIView.setAnimationBeginsFromCurrentState(true)

        // audioPlayer = AVAudioPlayer(contentsOfURL: popTileSound, error: nil)
        // audioPlayer.prepareToPlay()
        createNewGame()
	}
    
    func displayNewMessage(message: String)
    {
        userMessageLabel.text = message
    }
    
    func displayHighestRoundMessage(message: String)
    {
        highestRoundLabel.text = message
    }
    
    func displayCurrentPeeks(peekCount: Int)
    {
        var imageName: String = "\(peekCount)" + "Life"
        
        self.peekCount.setImage(UIImage(named: imageName), forState: UIControlState.Normal)
    }

    func playPopSound()
    {
        let popTileSound = NSBundle.mainBundle().pathForResource("PopEffect", ofType:"wav")
        let fileURL = NSURL(fileURLWithPath: popTileSound!)
        player = AVAudioPlayer(contentsOfURL: fileURL, error: nil)
        player.prepareToPlay()
        player.delegate = self
        player.play()
    }

    private func createGameTiles() -> [Tile]
    {
        var gameTiles = [Tile]()
        
        var buttons = [button1, button2, button3, button4, button5, button6, button7, button8, button9]

        for button in buttons
        {
            gameTiles.append(Tile(button: button))
        }
        
        return gameTiles
    }
    
    @IBAction func peekTiles(sender: UIButton)
    {
        game?.peekTiles()
    }
    
    @IBAction func unpeekTiles(sender: UIButton)
    {
        game?.unpeekTiles()
    }
    
    /// Check game round has been won after each button press.
    /// If game round has been won, set new round up
    /// Play button touch noise after each touch
	@IBAction func buttonTouched(sender: UIButton)
	{
        game?.changeTileAndCheckRound(sender.tag)
        
        playPopSound()
	}

    /// Create a new game and start a new round.
	@IBAction func resetTouched(sender: UIButton)
	{
        var leaderboard = Leaderboard()
        
        if let round = game?.highestRound
        {
            leaderboard.saveHighscore(round)
        }
    }
    
    /// Create a new game and start the round
    func createNewGame()
    {
        game = Game(tiles: createGameTiles(), messageDisplayer: self, difficultyDecider: StandardDifficulty())
        
        newRound()
    }
    
	func newRound()
	{
        game!.newRound()
    }
}