//
//  Tile.swift
//  Colourful Memory
//
//  Created by Ed Allonby on 15/12/2014.
//  Copyright (c) 2014 Ed Allonby. All rights reserved.
//

import UIKit

/// Houses logic for the tiles

class Tile
{
	private var button: UIButton

    private var colours = TileColours()
    
    var defaultColour: UIColor
    {
        return colours.defaultColour
    }
    
	var tag: Int
	{
		return button.tag
	}

	var currentColour: UIColor
	{
		return button.backgroundColor!
	}

	init(button: UIButton)
	{
		self.button = button
        
        self.button.layer.cornerRadius = 2
        self.button.layer.borderWidth = 1
        self.button.layer.borderColor = UIColor.blackColor().CGColor
        self.button.backgroundColor = defaultColour
	}

    /// Give the Tile a new set of available colours
    func newTileColours(tileColours: TileColours)
    {
        self.colours = tileColours
    }

	/// Makes the Tile the next available colour in its colour array
	func makeTileNextColour()
	{
        var nextUIColour = colours.getNextColour(button.backgroundColor!)

		changeColour(nextUIColour!)
	}
    
    func changeColour(colour: UIColor)
    {
        button.backgroundColor = colour
        
        animateTile()
    }
    
    func tileEnable(isEnabled: Bool)
    {
        button.enabled = isEnabled
    }

    /// Resets the Tile to its default colour, either with a fade animation or immediately
	func resetTile(isAnimated: Bool)
	{
		if (isAnimated)
		{
			UIView.animateWithDuration(2.0, animations: { () in self.resetTile() })
		}
		else
		{
			resetTile()
		}
	}
    
    private func animateTile()
    {
        let animationTime = 0.1
        let scaleMultiplier: CGFloat = 0.9

        UIView.animateWithDuration(animationTime,
            delay: 0,
            options: UIViewAnimationOptions.CurveEaseInOut | UIViewAnimationOptions.AllowUserInteraction,
            animations:
            {
                self.scaleTile(scaleMultiplier)
            },
            completion:
            {
                finished in UIView.animateWithDuration(animationTime,
                    delay: 0,
                    usingSpringWithDamping: 0.1,
                    initialSpringVelocity: 5,
                    options: UIViewAnimationOptions.CurveEaseInOut | UIViewAnimationOptions.AllowUserInteraction,
                    animations:
                    {
                          self.scaleTile(1.0)
                    },
                    completion: nil)
        })
    }
    
    private func scaleTile(scaleMultiplier: CGFloat)
    {
        self.button.transform = CGAffineTransformMakeScale(scaleMultiplier, scaleMultiplier)
    }
    
    private func resetTile()
    {
        button.backgroundColor = defaultColour
    }
}