//
//  UserMessage.swift
//  Colourful Memory
//
//  Created by Ed Allonby on 25/12/2014.
//  Copyright (c) 2014 Ed Allonby. All rights reserved.
//

import Foundation

struct UserMessage
{
    private static let wonMessagePart: String = "You won round "
    private static let repeatRoundPart: String = "Repeat - Round "
    private static let memoriseRoundPart: String = "Memorise - Round "
    private static let highestRoundPart: String = "Highest round: "
    
    static func wonMessage(roundNumber: Int) -> String
    {
        return wonMessagePart + "\(roundNumber)"
    }
    
    static func repeatRoundMessage(roundNumber: Int) -> String
    {
        return repeatRoundPart + "\(roundNumber)"
    }
    
    static func memoriseRoundMessage(roundNumber: Int) -> String
    {
        return memoriseRoundPart + "\(roundNumber)"
    }
    
    static func highestRoundMessage(roundNumber: Int) -> String
    {
        return highestRoundPart + "\(roundNumber)"
    }
}