//
//  Difficulty.swift
//  Colourful Memory
//
//  Created by Ed Allonby on 28/12/2014.
//  Copyright (c) 2014 Ed Allonby. All rights reserved.
//

import Foundation

/// Holds the information for difficulty settings
class Difficulty
{
    /// The current time the hidden tiles are displayed to the player.
    let waitTime: Double
    
    /// The number of coloured tiles the round has.
    let colouredTiles: Int
    
    /// The different colours in the round.
    let coloursForTiles: TileColours
    
    init(waitTime: Double, colouredTiles: Int, coloursForTiles: TileColours)
    {
        self.waitTime = waitTime
        self.colouredTiles = colouredTiles
        self.coloursForTiles = coloursForTiles
    }
}