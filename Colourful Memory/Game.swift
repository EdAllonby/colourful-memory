//
//  Game.swift
//  Colourful Memory
//
//  Created by Ed Allonby on 24/12/2014.
//  Copyright (c) 2014 Ed Allonby. All rights reserved.
//

import Foundation
import UIKit

/// A Game is a collection of rounds and button presses.
/// When a user gets a round correct, the game progresses to a new round.
/// When a user 'gives up', or resets the game, a new game is created.
class Game
{
    /// A game has a list of tiles.
    private var tiles = [Tile]()
    
    private var roundTracker = RoundTracker()
    
    private let peekTracker = PeekTracker()
    
    /// Update the UI with a new message
    private let messageDisplayer: MessageDisplayer
    
    /// A game has a current round which houses the hidden colour-tile combination
    private var currentRound: Round?
    
    /// Calculations for current round values
    private let difficultyDecider: DifficultyDecider
    
    var highestRound: Int
    {
            return roundTracker.currentRound
    }
    
    /// Create a new game with the tiles and a way to display messages to the user.
    init(tiles: [Tile], messageDisplayer: MessageDisplayer, difficultyDecider: DifficultyDecider)
    {
        self.tiles = tiles
        self.messageDisplayer = messageDisplayer
        self.difficultyDecider = difficultyDecider
        
        messageDisplayer.displayCurrentPeeks(peekTracker.peekCount)
        
        resetTiles(isEnabled: false, isAnimated: false)
    }
    
    /// Changes a tile to the next colour and checks for a win.
    func changeTileAndCheckRound(tileTag: Int)
    {
        tiles[tileTag].makeTileNextColour()
        handleRound()
    }
    
    func peekTiles()
    {
        if(peekTracker.removePeekIfAllowed())
        {            
            messageDisplayer.displayNewMessage(UserMessage.memoriseRoundMessage(roundTracker.currentRound))
            
            resetTiles(isEnabled: false, isAnimated: false)
            currentRound?.changeTilesToRoundColours(tiles)
            
            messageDisplayer.displayCurrentPeeks(peekTracker.peekCount)
        }
    }
    
    func unpeekTiles()
    {
        if(peekTracker.isPeeking)
        {
            peekTracker.stopPeeking()
            resetTilesForNewRound()
        }
    }
    
    /// Set the game up for a new round. This involves:
    /// - Increasing the round number by 1
    /// - Getting a new set of tile colours to use
    /// - Display new tile colours for a fixed time
    /// - After fixed time, reset tiles for user to begin round attempt
    @objc func newRound()
    {
        messageDisplayer.displayHighestRoundMessage(UserMessage.highestRoundMessage(RoundTracker.highestUnlockedRound))
        messageDisplayer.displayNewMessage(UserMessage.memoriseRoundMessage(roundTracker.currentRound))
        
        var newDifficulty: Difficulty = difficultyDecider.updateDifficulty(roundTracker.currentRound)

        currentRound = Round(tiles: tiles, numberOfColouredTiles: newDifficulty.colouredTiles, tileColours: newDifficulty.coloursForTiles)
        
        for tile in tiles
        {
            tile.newTileColours(newDifficulty.coloursForTiles)
        }
        
        println("New wait time is \(newDifficulty.waitTime)s.")
        println("Total coloured tiles is \(newDifficulty.colouredTiles).")
        
        var timer = NSTimer.scheduledTimerWithTimeInterval(newDifficulty.waitTime, target: self, selector: "resetTilesForNewRound", userInfo: nil, repeats: false)
    }
    
    @objc private func resetTilesForNewRound()
    {
        resetTiles(isEnabled: true, isAnimated: false)
        messageDisplayer.displayNewMessage(UserMessage.repeatRoundMessage(roundTracker.currentRound))
        println("User begun their attempt at making the colours match.")
    }
    
    private func resetTiles(#isEnabled: Bool, isAnimated: Bool)
    {
        for tile in tiles
        {
            tile.tileEnable(isEnabled)
            tile.resetTile(isAnimated)
        }
    }
    
    private func handleRound()
    {
        if(currentRound!.checkCurrentTiles(tiles))
        {
            messageDisplayer.displayNewMessage(UserMessage.wonMessage(roundTracker.currentRound))
            
            roundTracker.incrementRound()
            
            resetTiles(isEnabled: false, isAnimated: true)
            
            var timer = NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: "newRound", userInfo: nil, repeats: false)
            
            println("Round won!")
        }
        else
        {
            println("Round not won yet.")
        }
    }
}