//
//  TileTests.swift
//  Colourful Memory
//
//  Created by Ed Allonby on 23/12/2014.
//  Copyright (c) 2014 Ed Allonby. All rights reserved.
//

import UIKit
import XCTest

class TileTests: XCTestCase
{
    private var tile: Tile!
    private var tileColours: TileColours!
    
    override func setUp()
    {
        super.setUp()
        
        tile = Tile(button: UIButton())
        tileColours = TileColours()
        tile.newTileColours(tileColours)
    }
    
    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testChangeBackgroundChangesButtonBackgroundColour()
    {
        let colourToChangeTileTo = UIColor.blueColor()
        
        tile.changeColour(UIColor.blueColor())
        XCTAssertEqual(tile!.currentColour, colourToChangeTileTo)
    }
    
    func testTileGoesToNextTileColour()
    {
        var currentTileColour = tile.currentColour
        var nextExpectedTileColour = tileColours.getNextColour(currentTileColour)
        
        tile.makeTileNextColour()
        
        XCTAssertEqual(nextExpectedTileColour!, tile.currentColour)
    }
    
    func testTileResetsToDefaultColourWhenResetWithAnimation()
    {
        tile!.makeTileNextColour()
        XCTAssertNotEqual(tile.currentColour, tile.defaultColour)
        
        tile.resetTile(true)
        XCTAssertEqual(tile.currentColour, tile.defaultColour)
    }
    
    func testTileResetsToDefaultColourWhenResetWithoutAnimation()
    {
        tile.makeTileNextColour()
        XCTAssertNotEqual(tile.currentColour, tile.defaultColour)
        
        tile!.resetTile(false)
        XCTAssertEqual(tile.currentColour, tile.defaultColour)
    }
}