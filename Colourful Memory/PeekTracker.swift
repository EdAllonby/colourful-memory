//
//  PeekTracker.swift
//  Colourful Memory
//
//  Created by Ed Allonby on 27/12/2014.
//  Copyright (c) 2014 Ed Allonby. All rights reserved.
//

import Foundation

/// Holds the information for peeks within a Game
class PeekTracker
{
    /// The current amount of peeks available in a game.
    private(set) var peekCount: Int = 3
    
    /// Current status, if the game is using a peek.
    private(set) var isPeeking: Bool = false
    
    /// If there are peeks left, remove one and return true.
    /// Otherwise, do nothing.
    func removePeekIfAllowed() -> Bool
    {
        if peekCount > 0
        {
            peekCount--;
            isPeeking = true
            
            return true
        }
        
        return false
    }
    
    /// Make the PeekTracker know that the game is not peeking anymore.
    func stopPeeking()
    {
        isPeeking = false
    }    
}