//
//  RoundTracker.swift
//  Colourful Memory
//
//  Created by Ed Allonby on 24/12/2014.
//  Copyright (c) 2014 Ed Allonby. All rights reserved.
//

import Foundation

struct RoundTracker
{
    private(set) static var highestUnlockedRound: Int = 1
    
    private(set) var currentRound: Int = 1
    
    mutating func incrementRound()
    {
        currentRound++

        if currentRound > RoundTracker.highestUnlockedRound
        {
            RoundTracker.highestUnlockedRound = currentRound
        }
    }
}