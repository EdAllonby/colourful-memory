//
//  Round.swift
//  Colourful Memory
//
//  Created by Ed Allonby on 15/12/2014.
//  Copyright (c) 2014 Ed Allonby. All rights reserved.
//

import UIKit

/// Houses logic for a new game 'round'
class Round
{
    private var currentTileColoursIndexedByTileTag: [Int: UIColor] = Dictionary<Int, UIColor>()

    /// Create a new round with random Tile colours.
    init(tiles: [Tile], numberOfColouredTiles: Int, tileColours: TileColours)
    {
        currentTileColoursIndexedByTileTag = createNewTileColourArray(numberOfColouredTiles, tileColours: tileColours)
        
        changeTilesToRoundColours(tiles)
    }
    
    /// Compares the tiles parameter agaist the round's coloured tiles.
    /// If same, then returns true, else false.
	func checkCurrentTiles(tiles: [Tile]) -> Bool
	{
		var isAllCorrect: Bool = true

		for tile in tiles
		{
			if (!isTileColouredInRound(tile))
			{
				return false
			}

			for hiddenTile in currentTileColoursIndexedByTileTag
			{
				if (hiddenTile.0 == tile.tag)
				{
					if (hiddenTile.1 != tile.currentColour)
					{
						return false
					}
				}
			}
		}

		isAllCorrect = doesTilesMatchAllTransparentRound(tiles)

		return isAllCorrect
	}

    func changeTilesToRoundColours(tiles: [Tile])
    {
        for tile in tiles
        {
            for hiddenTile in currentTileColoursIndexedByTileTag
            {
                if (hiddenTile.0 == tile.tag)
                {
                    tile.changeColour(hiddenTile.1)
                    break
                }
            }
        }
    }
    
    private func createNewTileColourArray(numberOfColouredTiles: Int, tileColours: TileColours) -> [Int:UIColor]
    {
        var tileColoursByTag = Dictionary<Int, UIColor>()
        
        while (tileColoursByTag.count < numberOfColouredTiles)
        {
            let numberOfTiles = 9
            
            let tileTag = Int(arc4random_uniform(UInt32(numberOfTiles)))
            
            if (tileColoursByTag[tileTag] == nil)
            {
                tileColoursByTag[tileTag] = tileColours.randomColour()
            }
        }
        
        return tileColoursByTag
    }

	private func isTileColouredInRound(tile: Tile) -> Bool
	{
		return currentTileColoursIndexedByTileTag[tile.tag] != nil || tile.currentColour == UIColor.clearColor()
	}

	private func doesTilesMatchAllTransparentRound(tiles: [Tile]) -> Bool
	{
		var isAllTransparent: Bool = true

		var isAllCorrect: Bool = true

		// Edge case, if all are transparent, check that they were all supposed to be transparent.
		for tile in tiles
		{
			if (tile.currentColour != UIColor.clearColor())
			{
				isAllTransparent = false
				break
			}
		}
        
		if (isAllTransparent)
		{
			var areAllHiddenTilesTransparent: Bool = true

			for hiddenTileColour in currentTileColoursIndexedByTileTag.values
			{
				if (hiddenTileColour != UIColor.clearColor())
				{
					isAllCorrect = false
					break
				}
			}
		}

		return isAllCorrect
	}
}