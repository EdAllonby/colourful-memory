//
//  MenuViewController.swift
//  Colourful Memory
//
//  Created by Ed Allonby on 06/05/2015.
//  Copyright (c) 2015 Ed Allonby. All rights reserved.
//

import UIKit
import GameKit

class MenuViewController: UIViewController, GKGameCenterControllerDelegate
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        var leaderboard = Leaderboard()
        leaderboard.authenticateLocalPlayer()
    }
    
    @IBAction func OpenDeveloperWebsite(sender: AnyObject)
    {
        if let url = NSURL(string: "http://edallonby.co.uk")
        {
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
    @IBAction func showLeaderboard(sender: UIButton)
    {
        showLeaderboard()
    }
    
    //shows leaderboard screen
    func showLeaderboard()
    {
        var gc = GKGameCenterViewController()
        gc.gameCenterDelegate = self
        
        gc.viewState = GKGameCenterViewControllerState.Leaderboards
        gc.leaderboardIdentifier = "ColourfulMemory.Leaderboard"
        
        self.presentViewController(gc, animated: true, completion: nil)
    }
    
    //hides leaderboard screen
    func gameCenterViewControllerDidFinish(gameCenterViewController: GKGameCenterViewController!)
    {
        gameCenterViewController.dismissViewControllerAnimated(true, completion: nil)
    }
}