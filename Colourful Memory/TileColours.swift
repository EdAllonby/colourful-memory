//
//  TileColours.swift
//  Colourful Memory
//
//  Created by Ed Allonby on 16/12/2014.
//  Copyright (c) 2014 Ed Allonby. All rights reserved.
//

import UIKit

/// Holds the current colours allowed for Tiles.
class TileColours
{
    // Default colours
    private let red = UIColor(netHex: 0xc0392b)
    private let green = UIColor(netHex: 0x27ae60)
    private let blue = UIColor(netHex: 0x2980b9)
    private let clear = UIColor.clearColor()
    
    // Extra colours
    private let purple = UIColor(netHex: 0x9b59b6)
    private let orange = UIColor(netHex: 0xe67e22)
    
	private var colours = [UIColor]()
    
    var defaultColour: UIColor
    {
        return clear
    }
    
    init()
    {
        colours = [red, green, blue, clear]
    }
    
    func addNextColour()
    {
        if(!contains(colours, purple))
        {
            colours.insert(purple, atIndex: colours.count - 1)
            return
        }
        if(!contains(colours, orange))
        {
            colours.insert(orange, atIndex: colours.count - 1)
            return
        }
    }

	func getNextColour(currentColour: UIColor) -> UIColor?
	{
		var currentColourIndex = find(colours, currentColour)

		var nextColourIndex = ++currentColourIndex! % colours.count

		var nextUIColour: UIColor = colours[nextColourIndex]

		return nextUIColour
	}

	func randomColour() -> UIColor
	{
		let randomColourIndex = Int(arc4random_uniform(UInt32(colours.count - 1)))

		return colours[randomColourIndex]
	}
}