//
//  DifficultyDecider.swift
//  Colourful Memory
//
//  Created by Ed Allonby on 28/12/2014.
//  Copyright (c) 2014 Ed Allonby. All rights reserved.
//

import Foundation

/// Computes the current round's difficulty.
protocol DifficultyDecider
{
    /// Update the Difficulty settings based on the round number.
    func updateDifficulty(roundNumber: Int) -> Difficulty
}