//
//  Leaderboard.swift
//  Colourful Memory
//
//  Created by Ed Allonby on 06/05/2015.
//  Copyright (c) 2015 Ed Allonby. All rights reserved.
//

import GameKit

class Leaderboard : UIViewController
{
    //send high score to leaderboard
    func saveHighscore(score:Int) {
        
        //check if user is signed in
        if GKLocalPlayer.localPlayer().authenticated
        {
            var scoreReporter = GKScore(leaderboardIdentifier: "ColourfulMemory.Leaderboard")
            
            scoreReporter.value = Int64(score)
            
            var scoreArray: [GKScore] = [scoreReporter]
            
            GKScore.reportScores(scoreArray, withCompletionHandler:
            {
                (error : NSError!) -> Void in
                
                if error != nil
                {
                    println("error")
                }
            })
        }
    }
    
    //initiate gamecenter
    func authenticateLocalPlayer()
    {
        var localPlayer = GKLocalPlayer.localPlayer()
        
        localPlayer.authenticateHandler = {(viewController, error) -> Void in
            
            if (viewController != nil)
            {
                self.presentViewController(viewController, animated: true, completion: nil)
            }
            else
            {
                println("Logged in to Game Center: " + "\(GKLocalPlayer.localPlayer().authenticated)")
            }
        }
    }
}
